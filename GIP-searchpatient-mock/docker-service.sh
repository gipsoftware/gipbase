#!/bin/bash
# - P A R A M E T E R S
COMMAND=$1
ENVIRONMENT=$2
# - C O N S T A N T S
CONTAINER_NAME="innovative/backend.search.patients"
JAR_PREFIX="GIP-searchpatient-mock"
JAR_NAME=${JAR_PREFIX}".jar"
WORKING_DIRECTORY="$(dirname "$0")"
DOCKER_DIRECTORY="${WORKING_DIRECTORY}/src/main/resources/docker"
DOCKER_COMPOSER_COMMAND="docker-compose --file src/main/scripts/docker-${ENVIRONMENT}/docker-compose.yml"

# - G E N E R A T E   C O N T A I N E R
generateContainer() {
  cd "${WORKING_DIRECTORY}" || exit 1;
  rm -v "${DOCKER_DIRECTORY}"/*.jar
  ./gradlew clean bootJar
  cp ./build/libs/*.jar "$DOCKER_DIRECTORY"
  cd "$DOCKER_DIRECTORY" || exit 1;
  mv -v ${JAR_PREFIX}*.jar ${JAR_NAME}
  echo "${DOCKER_DIRECTORY}/Dockerfile"
  docker build -t ${CONTAINER_NAME} .
}
# - S T A R T / S T O P
start() {
  cd "${WORKING_DIRECTORY}" || exit 1;
  RUN_COMMAND="${DOCKER_COMPOSER_COMMAND}"
  $RUN_COMMAND up &
}
stop() {
  cd "${WORKING_DIRECTORY}" || exit 1;
  RUN_COMMAND="${DOCKER_COMPOSER_COMMAND}"
  $RUN_COMMAND down
}
recycle() {
  # - STOP
  cd "${WORKING_DIRECTORY}" || exit 1;
  RUN_COMMAND="${DOCKER_COMPOSER_COMMAND}"
  $RUN_COMMAND down
  generateContainer
  cd "${WORKING_DIRECTORY}" || exit 1;
  RUN_COMMAND="${DOCKER_COMPOSER_COMMAND}"
  $RUN_COMMAND up &
}

case $COMMAND in
'generate')
  generateContainer
  ;;
'start')
  start
  ;;
'stop')
  stop
  ;;
'recycle')
  recycle
  ;;
*)
  echo "Usage: $0 { generate | start | stop | recycle }"
  echo
  exit 1
  ;;
esac
exit 0
