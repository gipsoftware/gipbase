package org.innovative.dimension.gip.backend.patient.persistence;

import java.util.Objects;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;

@Entity(name = "patients")
public class PatientEntity {
	public static String normalizeSearchData( final String search ) {
		if (null != search)
			return search.toLowerCase()
					.replace( 'á', 'a' )
					.replace( 'é', 'e' )
					.replace( 'í', 'i' )
					.replace( 'ó', 'o' )
					.replace( 'ú', 'u' )
					.replace( 'à', 'a' )
					.replace( 'à', 'e' )
					.replace( 'ì', 'i' )
					.replace( 'ò', 'o' )
					.replace( 'ù', 'u' );
		else return null;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", insertable = false, updatable = false, nullable = false)
	private UUID id;
	@Size(min = 3, max = 50)
	@Column(name = "name", updatable = true, nullable = false)
	private String name;
	@Size(min = 3, max = 200)
	@Column(name = "surname", updatable = true, nullable = false)
	private String surname;
	@Size(min = 9, max = 9)
	@Column(name = "dni", updatable = true, nullable = false)
	private String dni;
	@Size(min = 3, max = 259)
	@Column(name = "search_data", updatable = true, nullable = false)
	private String searchData;

	// - C O N S T R U C T O R S
	private PatientEntity() {}

	// - G E T T E R S   &   S E T T E R S
	public String getDni() {
		return this.dni;
	}

	public UUID getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public String getSurname() {
		return this.surname;
	}

	public String getSearchData() {
		return this.searchData;
	}

	private void updateSearchData() {
		this.searchData = normalizeSearchData( this.name ) + " " +
				normalizeSearchData( this.surname ) + " " +
				normalizeSearchData( this.dni );
	}

	// - B U I L D E R
	public static class Builder {
		private final PatientEntity onConstruction;

		// - C O N S T R U C T O R S
		public Builder() {
			this.onConstruction = new PatientEntity();
		}

		public PatientEntity build() {
			Objects.requireNonNull( this.onConstruction.name );
			Objects.requireNonNull( this.onConstruction.surname );
			Objects.requireNonNull( this.onConstruction.dni );
			this.onConstruction.id = UUID.randomUUID();
			return this.onConstruction;
		}

		public PatientEntity.Builder withDNI( final String dni ) {
			this.onConstruction.dni = Objects.requireNonNull( dni );
			this.onConstruction.updateSearchData();
			return this;
		}

		public PatientEntity.Builder withName( final String name ) {
			this.onConstruction.name = Objects.requireNonNull( name );
			this.onConstruction.updateSearchData();
			return this;
		}

		public PatientEntity.Builder withSurName( final String surname ) {
			this.onConstruction.surname = Objects.requireNonNull( surname );
			this.onConstruction.updateSearchData();
			return this;
		}
	}
}
