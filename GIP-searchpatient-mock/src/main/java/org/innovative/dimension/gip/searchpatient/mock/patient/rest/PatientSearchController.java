package org.innovative.dimension.gip.searchpatient.mock.patient.rest;

import java.text.MessageFormat;
import java.util.Optional;
import javax.validation.constraints.NotNull;

import org.innovative.dimension.gip.backend.patient.persistence.PatientEntity;
import org.innovative.dimension.gip.backend.patient.persistence.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import org.dimensinfin.logging.LogWrapper;

@RestController
@Validated
@RequestMapping("/api/v1")
public class PatientSearchController {
	private final PatientRepository patientRepository;

	// - C O N S T R U C T O R S
	@Autowired
	public PatientSearchController( final @NotNull PatientRepository patientRepository ) {this.patientRepository = patientRepository;}

	@GetMapping(path = "/patients/search",
			consumes = "application/json",
			produces = "application/json")
	public ResponseEntity<Page<PatientEntity>> searchPatients( final @RequestParam(name = "data") String data,
	                                                           //	                                                           final Pageable pageable
	                                                           final @RequestParam(name = "page") Optional<Integer> page,
	                                                           final @RequestParam(name = "size") Optional<Integer> size ) {

		LogWrapper.enter( MessageFormat.format( "Page: {0} - Size: {1}", page.orElse( 0 ), size.orElse( 5 ) ) );
		LogWrapper.info( "Search data: " + PatientEntity.normalizeSearchData( data ) );
		return new ResponseEntity<>( this.searchPatients4Data( PatientEntity.normalizeSearchData( data ),
				page, size ),
				HttpStatus.OK );
		//		this.patientRepository.findBySearchDataContainingIgnoreCase( PatientEntity.normalizeSearchData( data ),
		//				pageable/*,
		//				 Sort.by( "searchData" ).ascending()*/ ),


		//				new PageImpl( this.searchPatients4Data( data, page, size ),
		//				PageRequest.of( page.orElse( 0 ), size.orElse( 5 ),
		//						Sort.by( "searchData" ).ascending() ),
		//				100 ),
		//				HttpStatus.OK );
	}

	private Page<PatientEntity> searchPatients4Data( final String searchData, final Optional<Integer> page, final Optional<Integer> size ) {
		return this.patientRepository.findBySearchDataContainingIgnoreCase( searchData,
				PageRequest.of( page.orElse( 0 ), size.orElse( 5 ),
						Sort.by( "searchData" ).ascending() ) );
	}
}
