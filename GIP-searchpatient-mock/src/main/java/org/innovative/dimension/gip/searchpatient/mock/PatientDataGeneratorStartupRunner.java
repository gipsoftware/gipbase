package org.innovative.dimension.gip.searchpatient.mock;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import org.innovative.dimension.gip.backend.patient.persistence.PatientEntity;
import org.innovative.dimension.gip.backend.patient.persistence.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import org.dimensinfin.logging.LogWrapper;

@Component
public class PatientDataGeneratorStartupRunner implements CommandLineRunner {
	private static final List<String> names = new ArrayList<>();
	private static final List<String> apellidos = new ArrayList<>();
	private static final String juegoCaracteres = "TRWAGMYFPDXBNJZSQVHLCKE";

	static {
		names.add( "Ana" );
		names.add( "Berta" );
		names.add( "Cristina" );
		names.add( "Dafne" );
		names.add( "Elisa" );
		names.add( "Helena" );
		names.add( "Alberto" );
		names.add( "Carlos" );
		names.add( "Demetrio" );
		names.add( "Enrique" );
		names.add( "Francisco" );
		names.add( "Gonzalo" );
	}

	static {
		apellidos.add( "Álvarez" );
		apellidos.add( "Benitez" );
		apellidos.add( "Diego" );
		apellidos.add( "Fernandez" );
		apellidos.add( "García" );
		apellidos.add( "Gómez" );
		apellidos.add( "López" );
		apellidos.add( "Martínez" );
		apellidos.add( "Movillo" );
		apellidos.add( "Narvaez" );
		apellidos.add( "Ordoñez" );
		apellidos.add( "Pérez" );
		apellidos.add( "Rodríguez" );
		apellidos.add( "Sánchez" );
		apellidos.add( "Tordesillas" );
		apellidos.add( "Urbis" );
		apellidos.add( "Valdez" );
		apellidos.add( "Zambrano" );
	}

	@Autowired
	private PatientRepository patientRepository;
	private Integer dniStart = 85000001;

	/**
	 * Insert the Patient data record into the mock database.
	 * Generate a record for each combination of [apellido-apellido-name]. Then generate a new DNI and phone number.
	 */
	@Override
	public void run( String... args ) throws Exception {
		LogWrapper.enter();
		this.patientRepository.deleteAll();
		for (int i = 0; i < apellidos.size(); i++) {
			for (int j = 0; j < apellidos.size(); j++) {
				for (int k = 0; k < names.size(); k++) {
					final PatientEntity patient = this.generatePatient( i, j, k );
//					LogWrapper.info( MessageFormat.format( "Saving Patient: {0} with dni: {1}",
//							patient.getFullName(),
//							patient.getDni() ) );
					LogWrapper.info( MessageFormat.format( "Saving Patient: {0}",patient.getSearchData() ) );
					this.patientRepository.save( patient );
				}
			}
		}
		LogWrapper.exit();
	}

	private char calculateDNICharacter( final int dni ) {
		int modulo = dni % 23;
		char letra = juegoCaracteres.charAt( modulo );
		return letra;
	}

	private String generateApellidos( final int i, final int j ) {
		return apellidos.get( i ) + " " + apellidos.get( j );
	}

	private String generateDNI() {
		final String dni = this.dniStart.toString() + this.calculateDNICharacter( this.dniStart );
		this.dniStart++;
		return dni;
	}

	private String generateName( final int k ) {
		return names.get( k );
	}

	private PatientEntity generatePatient( final int i, final int j, final int k ) {
		final String name = this.generateName( k );
		final String apellidos = this.generateApellidos( i, j );
		final String dni = this.generateDNI();
		return new PatientEntity.Builder()
				.withName( name )
				.withSurName( apellidos )
				.withDNI( dni )
				.build();
	}
}
