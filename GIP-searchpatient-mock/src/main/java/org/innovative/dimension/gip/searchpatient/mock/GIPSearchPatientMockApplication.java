package org.innovative.dimension.gip.searchpatient.mock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("org.innovative.dimension.gip.backend.patient.persistence")
@EntityScan("org.innovative.dimension.gip.backend.patient.persistence")
public class GIPSearchPatientMockApplication {
	public static void main(String[] args) {
		SpringApplication.run( GIPSearchPatientMockApplication.class, args);
	}
}
