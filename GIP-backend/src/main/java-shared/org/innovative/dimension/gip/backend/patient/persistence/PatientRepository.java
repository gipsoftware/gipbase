package org.innovative.dimension.gip.backend.patient.persistence;

import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "patients", path = "patients")
public interface PatientRepository extends PagingAndSortingRepository<PatientEntity, UUID> {
	Page<PatientEntity> findBySearchDataContainingIgnoreCase( final String data, final Pageable pageable );
}
