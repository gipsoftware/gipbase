package com.example.demo.persistence;

public class PatientEntity {
	private String name;

	public String getName() {
		return name;
	}

	private PatientEntity() {}

	// - B U I L D E R
	public static class Builder {
		private final PatientEntity onConstruction;

		public Builder() {
			this.onConstruction = new PatientEntity();
		}

		public PatientEntity build() {
			return this.onConstruction;
		}
	}
}
