package com.example.demo.persistence;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

@RestResource(exported = true)
public interface PatientRepository extends JpaRepository<PatientEntity, UUID> {
}
