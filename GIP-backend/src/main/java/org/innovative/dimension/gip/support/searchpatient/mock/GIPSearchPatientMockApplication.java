package org.innovative.dimension.gip.support.searchpatient.mock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GIPSearchPatientMockApplication {

	public static void main(String[] args) {
		SpringApplication.run( GIPSearchPatientMockApplication.class, args);
	}

}
