package org.innovative.dimension.gip.support.searchpatient.mock;

import org.springframework.boot.CommandLineRunner;

public class PatientDataGeneratorStartupRunner implements CommandLineRunner {
	/**
	 * Insert the Patient data record into the mock database.
	 * Generate a record for each combination of [apellido-apellido-name]. Then generate a new DNI and phone number.
 	 * @param args
	 * @throws Exception
	 */
	@Override
	public void run( String... args ) throws Exception {

	}
}
