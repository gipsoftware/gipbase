export const environment = {
    production: false,
    showexceptions: true,
    copyright: '© 2020 Dimensinfin Industries',
    appTitle: '3DPrinterManagement - UI',
    appName: require('../../package.json').name,
    appVersion: require('../../package.json').version,
    appSignature: "S000.02.001-20200518",
    platform: 'Angular 10.0.11 - RxJs 6.5.5 - Rollbar 2.19.2',
    backendPath: '',
    apiVersion1: '/api/v1'
};
