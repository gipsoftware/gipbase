// - CORE
import { Node } from '@innovative/domain/core/Node.domain'

export class Patient extends Node {
    private id: string
    private name: string
    private surname: string
    private dni: string
    private searchData: string

    constructor(values: Object = {}) {
        super()
        Object.assign(this, values)
        this.jsonClass = 'Patient'
    }
    // - GETTERS & SETTERS
    public getUniqueId(): string {
        return this.id
    }
    public getDNI(): string {
        return this.dni
    }
    public getFullName(): string {
        return this.surname + ', ' + this.name
    }
    public getSearchData(): string {
        return this.searchData
    }
}
