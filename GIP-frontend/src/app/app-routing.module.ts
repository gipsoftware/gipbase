// - CORE MODULES
import { NgModule } from '@angular/core';
// - ROUTING
import { Routes } from '@angular/router';
import { RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        redirectTo: '/patient/list',
        pathMatch: 'full',
        data: {
            breadcrumb: '/patient'
        }
    },
    { path: 'patient', loadChildren: () => import('./modules/patient/patient.module').then(m => m.PatientModule) }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
