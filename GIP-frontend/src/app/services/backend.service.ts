// - CORE
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';
// - SERVICES
import { HttpClientWrapperService } from '@app/services/httpclientwrapper.service';
import { ResponseTransformer } from './support/ResponseTransformer';
import { environment } from '@env/environment';
// - INNOVATIVE
import { BackendInfoResponse } from '@innovative/domain/dto/BackendInfoResponse.dto';
import { Pager } from '@innovative/domain/dto/Pager.dto';
// - DOMAIN
import { Patient } from '@domain/Patient.domain';

@Injectable({
    providedIn: 'root'
})
export class BackendService {
    private APIV1: string;

    constructor(
        protected httpService: HttpClientWrapperService) {
        this.APIV1 = environment.backendPath + environment.apiVersion1;
    }
    // - A C T U A T O R - A P I
    public apiActuatorInfo(transformer: ResponseTransformer): Observable<BackendInfoResponse> {
        const request = environment.backendPath + '/actuator/info';
        let headers = new HttpHeaders()
            .set('xapp-name', environment.appName);
        return this.httpService.wrapHttpGETCall(request, headers)
            .pipe(map((data: any) => {
                console.log(">[BackendService.apiActuatorInfo]> Transformation: " + transformer.description);
                const response = transformer.transform(data) as BackendInfoResponse;
                return response;
            }));
    }
    // - B A C K E N D - A P I
    // - S E A R C H   P A T I E N T S
    public apiSearchPatient4Data(data: string/*, transformer: ResponseTransformer*/): Observable<Pager> {
        const request = this.APIV1 + '/patients/search?page=0&size=50&data=' + data;
        let headers = new HttpHeaders()
            .set('xapp-name', environment.appName);
        return this.httpService.wrapHttpGETCall(request, headers)
            .pipe(map((data: Pager) => {
                return new Pager(data);
            }));
    }
}
