// - CORE MODULES
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
// - SERVICES
import { BackendService } from './services/backend.service';

@NgModule({
    imports: [
        BrowserModule,
        DragDropModule,
        AppRoutingModule,
        HttpClientModule
    ],
    declarations: [
        AppComponent
    ],
    providers: [
        // - SERVICES
        { provide: BackendService, useClass: BackendService },
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
