import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { V1VerbRenderComponent } from './v1-verb-render.component';

describe('V1VerbRenderComponent', () => {
  let component: V1VerbRenderComponent;
  let fixture: ComponentFixture<V1VerbRenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ V1VerbRenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(V1VerbRenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
