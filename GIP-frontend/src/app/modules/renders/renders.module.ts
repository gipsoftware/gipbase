// - CORE MODULES
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DragDropModule } from '@angular/cdk/drag-drop';
// - COMPONENTS
import { NodeContainerRenderComponent } from './node-container-render/node-container-render.component';
import { V1VerbRenderComponent } from './v1-verb-render/v1-verb-render.component';
import { V1PatientRenderComponent } from './v1-patient-render/v1-patient-render.component';

@NgModule({
    imports: [
        CommonModule,
        DragDropModule
    ],
    declarations: [
        NodeContainerRenderComponent,
        V1VerbRenderComponent,
        V1PatientRenderComponent],
    exports: [
        NodeContainerRenderComponent,
        V1PatientRenderComponent
    ]
})
export class RendersModule { }
