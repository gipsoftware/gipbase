// - CORE
import { Component } from '@angular/core';
import { Input } from '@angular/core';
import { NodeContainerRenderComponent } from '../node-container-render/node-container-render.component';
// - DOMAIN
import { Patient } from '@domain/Patient.domain';

@Component({
    selector: 'v1-patient',
    templateUrl: './v1-patient-render.component.html',
    styleUrls: ['./v1-patient-render.component.scss']
})
export class V1PatientRenderComponent extends NodeContainerRenderComponent {
    public getNode(): Patient {
        return this.node as Patient
    }
    public getUniqueId(): string {
        return this.getNode().getUniqueId()
    }
    public getFullName(): string {
        return this.getNode().getFullName()
    }
    public getDNI():string{
        return this.getNode().getDNI()
    }
}
