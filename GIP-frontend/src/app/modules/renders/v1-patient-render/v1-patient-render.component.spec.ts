import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { V1PatientRenderComponent } from './v1-patient-render.component';

describe('V1PatientRenderComponent', () => {
  let component: V1PatientRenderComponent;
  let fixture: ComponentFixture<V1PatientRenderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ V1PatientRenderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(V1PatientRenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
