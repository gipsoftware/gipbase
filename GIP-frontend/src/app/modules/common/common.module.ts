// - CORE MODULES
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// - COMPONENTS
import { SearchBarPanelComponent } from './panels/search-bar-panel/search-bar-panel.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [SearchBarPanelComponent],
  exports: [SearchBarPanelComponent]
})
export class AppCommonModule { }
