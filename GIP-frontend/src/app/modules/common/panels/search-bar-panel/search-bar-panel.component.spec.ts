import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchBarPanelComponent } from './search-bar-panel.component';

describe('SearchBarPanelComponent', () => {
  let component: SearchBarPanelComponent;
  let fixture: ComponentFixture<SearchBarPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchBarPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchBarPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
