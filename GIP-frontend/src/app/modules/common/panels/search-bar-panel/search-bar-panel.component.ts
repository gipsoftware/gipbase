// - CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { OnDestroy } from '@angular/core';
import { Input } from '@angular/core';
import { Subscription } from 'rxjs';

@Component({
    selector: 'search-bar-panel',
    templateUrl: './search-bar-panel.component.html',
    styleUrls: ['./search-bar-panel.component.scss']
})
export class SearchBarPanelComponent {
    @Output() searchcriteria = new EventEmitter<String>()
    public searchword: string = ''

    constructor() { }

    public searchThis() {
        if (null != this.searchword)
            if (this.searchword.length > 2) {
                console.log('-[SearchBarPanelComponent.searchThis]> Filter: ' + this.searchword)
                this.searchcriteria.emit(this.searchword)
            }
    }
}
