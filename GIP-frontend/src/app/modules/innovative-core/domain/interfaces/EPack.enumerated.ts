/**
 * Declares the different display identifiers that can be used to differentiate different renders for a same entity class. With the use of 'variants' the same model can be rendered differently depending on the view panel where it is stored.
*/
export enum EVariant {
    DEFAULT = '-DEFAULT-'
}
