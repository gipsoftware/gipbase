export class Pager {
    public numberOfElements: number
    public content: any[]
    public size: number
    public number: number
    public pageable: object
    public totalPages: number
    public totalElements: number
    public empty: boolean
    public first: boolean
    public last: boolean

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
    // - GETTERS & SETTERS
    public getContents(): any[] {
        return this.content
    }
}
