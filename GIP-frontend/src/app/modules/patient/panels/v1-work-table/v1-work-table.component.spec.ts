import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { V1WorkTableComponent } from './v1-work-table.component';

describe('V1WorkTableComponent', () => {
  let component: V1WorkTableComponent;
  let fixture: ComponentFixture<V1WorkTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ V1WorkTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(V1WorkTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
