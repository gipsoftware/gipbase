// - CORE
import { Component } from '@angular/core';
import { OnInit } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { OnDestroy } from '@angular/core';
import { Input } from '@angular/core';
import { Subscription } from 'rxjs';
// - SERVICES
import { BackendService } from '@app/services/backend.service';
// - INNOVATIVE
import { AppPanelComponent } from '@innovative/components/app-panel/app-panel.component';
import { Pager } from '@innovative/domain/dto/Pager.dto';
// - DOMAIN
import { Patient } from '@domain/Patient.domain';
import { ResponseTransformer } from '@innovative/services/support/ResponseTransformer';

const transformer: ResponseTransformer = new ResponseTransformer()
    .setDescription('Convert paged Patient records into Patient instances.')
    .setTransformation((records: any[]) => {
        const patients: Patient[] = []
        for (let record of records) {
            patients.push(new Patient(record))
        }
        return patients
    })

@Component({
    selector: 'v1-patients-panel',
    templateUrl: './v1-patients-panel.component.html',
    styleUrls: ['./v1-patients-panel.component.scss']
})
export class V1PatientsPanelComponent extends AppPanelComponent implements OnInit {
    @Input() filter: string
    private patients: Patient[] = []
    public total: number = 0

    constructor(protected backendService: BackendService) {
        super();
        this.patients.push(new Patient({ searchData: '12' }))
        this.patients.push(new Patient({ searchData: '23' }))
        this.patients.push(new Patient({ searchData: '31' }))
        this.patients.push(new Patient({ searchData: '123' }))
        this.patients.push(new Patient({ searchData: '1234' }))
        this.patients.push(new Patient({ searchData: '1235' }))
    }

    // - L I F E C Y C L E
    public ngOnInit(): void {
        console.log(">[V1PatientsPanelComponent.ngOnInit]");
        this.startDownloading();
        this.downloadPatients()
        console.log("<[V1PatientsPanelComponent.ngOnInit]");
    }
    // public getNodes2Render(): ICollaboration[] {
    //     return []
    // }
    public refresh(): void {
        console.log(">[V1PatientsPanelComponent.refresh]");
        this.downloadPatients()
        console.log("<[V1PatientsPanelComponent.refresh]");
    }
    private downloadPatients(): void {
        if (null != this.filter)
            if (this.filter.length > 2) {
                const dataFilter = this.filter
                console.log("-[V1PatientsPanelComponent.downloadPatients]> Data filter: " + dataFilter);
                this.backendConnections.push(this.backendService.apiSearchPatient4Data(dataFilter)
                    .subscribe((pager: Pager) => {
                        console.log("-[V1PatientsPanelComponent.downloadPatients]> First Page: " + pager.first);
                        console.log("-[V1PatientsPanelComponent.downloadPatients]> Page size: " + pager.numberOfElements);
                        console.log("-[V1PatientsPanelComponent.downloadPatients]> Total: " + pager.totalElements);
                        this.completeDowload(transformer.transform(pager.getContents()))
                        this.total = pager.totalElements
                    })
                )
                let content = this.patients.filter(function (ele: Patient, i, array) {
                    let arrayelement = ele.getSearchData().toLowerCase()
                    return arrayelement.includes(dataFilter)
                })
                console.log("-[V1PatientsPanelComponent.downloadPatients]");
                this.completeDowload(content)
            }
    }
}
