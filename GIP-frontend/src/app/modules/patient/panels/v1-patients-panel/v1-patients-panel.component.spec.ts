import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { V1PatientsPanelComponent } from './v1-patients-panel.component';

describe('V1PatientsPanelComponent', () => {
  let component: V1PatientsPanelComponent;
  let fixture: ComponentFixture<V1PatientsPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ V1PatientsPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(V1PatientsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
