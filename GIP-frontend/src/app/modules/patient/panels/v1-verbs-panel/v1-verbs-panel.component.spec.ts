import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { V1VerbsPanelComponent } from './v1-verbs-panel.component';

describe('V1VerbsPanelComponent', () => {
  let component: V1VerbsPanelComponent;
  let fixture: ComponentFixture<V1VerbsPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ V1VerbsPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(V1VerbsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
