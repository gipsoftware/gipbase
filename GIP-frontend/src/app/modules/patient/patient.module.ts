// - CORE MODULES
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { DragDropModule } from '@angular/cdk/drag-drop';
// - APPLICATION MODULES
import { AppCommonModule } from '../common/common.module';
// - ROUTING
import { Routes } from '@angular/router';
import { RouterModule } from '@angular/router';
// - COMPONENTS
import { PatientListPageComponent } from './pages/patient-list-page/patient-list-page.component';
import { V1VerbsPanelComponent } from './panels/v1-verbs-panel/v1-verbs-panel.component';
import { V1PatientsPanelComponent } from './panels/v1-patients-panel/v1-patients-panel.component';
import { RendersModule } from '../renders/renders.module';
import { V1WorkTableComponent } from './panels/v1-work-table/v1-work-table.component';

const routes: Routes = [
    {
        path: 'list', component: PatientListPageComponent,
        data: {
            breadcrumb: '/list'
        }
    }
];

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        FormsModule,
        DragDropModule,
        AppCommonModule,
        RendersModule
    ],
    declarations: [
        PatientListPageComponent,
        V1VerbsPanelComponent,
        V1PatientsPanelComponent,
        V1WorkTableComponent
    ],
    exports: [
        RouterModule,
        V1VerbsPanelComponent
    ]
})
export class PatientModule { }
