// - CORE
import { Component } from '@angular/core';
import { ViewChild } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { OnDestroy } from '@angular/core';
import { Input } from '@angular/core';
import { Subscription } from 'rxjs';
// - DOMAIN
import { V1PatientsPanelComponent } from '../../panels/v1-patients-panel/v1-patients-panel.component';

@Component({
    selector: 'patient-list-page',
    templateUrl: './patient-list-page.component.html',
    styleUrls: ['./patient-list-page.component.scss']
})
export class PatientListPageComponent {
    @ViewChild(V1PatientsPanelComponent) private patientsPanel: V1PatientsPanelComponent;
    public patientFilter: string

    public getFilteredPatients(data: string) {
        this.patientFilter = data
        this.patientsPanel.refresh()
    }
}
