// - CORE
import { environment } from '../../src/environments/environment';

export class SupportService {
    private routeTranslationTable: any = {}
    private translationTable: any = {}

    constructor() {
        // - ROUTES
        this.routeTranslationTable['Patients/List'] = '/patient/list'
        // - PAGES
        this.translationTable['Patients List Page'] = 'patient-list-page'
        // - PANELS
        this.translationTable['app-component'] = 'app-root'
        this.translationTable['Search Bar'] = 'search-bar-panel'
        this.translationTable['Verbs'] = 'v1-verbs-panel'
        this.translationTable['Patients'] = 'v1-patients-panel'
        // - RENDERS
        this.translationTable['verb'] = 'v1-verb'
        this.translationTable['patient'] = 'v1-patient'
        // - TAGS
        this.translationTable['buttons'] = 'button'
        // - BACKEND REQUESTS
        this.translationTable['Get Open Requests'] = 'apiProductionGetOpenRequests_v2'
        this.translationTable['Start Build Job'] = 'apiMachinesStartBuild_v2'
        this.translationTable['Save New Part'] = 'apiNewPart_v1'
    }
    /**
     * Replaces symbolic names by the application names so if there are version changes the acceptance scritps should not change.
     * For example the tags for machine can change from version 1 v1-machine to version 3 v3-machine without changes on the test code.
     * @param tag the HTML tag name to serch for the applciation tag replacement.
     */
    public translateTag(name: string): string {
        return this.translationTable[name];
    }
    public translateRoute(simbolicRoute: string): string {
        return this.routeTranslationTable[simbolicRoute];
    }
    // - R A N D O M
    public generateRandomNum(min: number, max: number) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };
    public generateRandomString(length: number): string {
        var string = '';
        var letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz' //Include numbers if you want
        for (let i = 0; i < length; i++) {
            string += letters.charAt(Math.floor(Math.random() * letters.length));
        }
        return string;
    }
    // - T E M P L A T E   R E P L A C E M E N T
    /**
    * This function replaces values found on Gherkin files by configuration values if they fit the <name> syntax.
    * There are 3 sets of templated values. Environmental that will replace the value by an 'environtment' value,
    * configuration that will do the same with an application configured constant and system that will replace the
    * value by the result of a system function.
    * @param value the value to check for templated.
    */
    public replaceValueTemplated(value: string): string {
        let regex = /^<(.+)\.(.+)>$/g
        if (regex.test(value)) {
            const domain = RegExp.$1;
            const name = RegExp.$2;
            console.log('-[replaceValueTemplated]>domain=' + domain);
            console.log('-[replaceValueTemplated]>name=' + name);
            if (null != domain) {
                switch (domain) {
                    case 'environment':
                        return this.replaceEnvironmentTemplate(name);
                        break;
                }
            }
        }
        return value;
    }
    public replaceEnvironmentTemplate(templateName: string): string {
        switch (templateName) {
            case 'app-name':
                return environment.appName;
            case 'app-title':
                return environment.appTitle;
            case 'app-version':
                return environment.appVersion;
            case 'copyright':
                return environment.copyright;
        }
        return '-undefined-';
    }
}
