// - CUCUMBER-PREPROCESSOR
import { Given } from "cypress-cucumber-preprocessor/steps";
import { When } from "cypress-cucumber-preprocessor/steps";
import { Then } from "cypress-cucumber-preprocessor/steps";
import { Before } from "cypress-cucumber-preprocessor/steps";
// - SERVICE
import { SupportService } from '../../support/SupportService.support';

const supportService = new SupportService();

// - S T E P   M A C R O S
Given('{string} as search filter', function (searchFilter: string) {
    const tag = supportService.translateTag("Search Bar") // Do name replacement
    cy.get('@target-page').find(tag)
        .as('target-panel').as('target')
    const fieldName = "search-filter"
    const fieldLabel = "FILTRO:"
    cy.get('@target-panel').get('[cy-name="' + fieldName + '"]').as('target-field')
    cy.get('@target-field').find('[cy-field-label="' + fieldName + '"]')
        .contains(fieldLabel, { matchCase: false })
    cy.get('@target-field').find('[cy-field-label="' + fieldName + '"]').invoke('attr', 'cy-input-type').then(type => {
        switch (type) {
            case 'input':
                cy.log('input')
                cy.get('@target-field').find('input')
                    .should('be.empty')
                break
            case 'select':
                cy.log('select')
                cy.get('@target-field').find('select')
                    .should('not.have.value')
                break
            case 'textarea':
                cy.log('select')
                cy.get('@target-field').find('textarea')
                    .should('be.empty')
                break
        }
    })
    const fieldValue = "123"
    cy.get('@target-panel').find('[cy-name="' + fieldName + '"]').as('target-field')
    cy.get('@target-field').find('[cy-field-label="' + fieldName + '"]').invoke('attr', 'cy-input-type').then(type => {
        switch (type) {
            case 'input':
                cy.get('@target-field').find('input').clear().type(fieldValue)
                break
            case 'textarea':
                cy.get('@target-field').find('textarea').clear().type(fieldValue)
                break
            case 'select':
                cy.log('select')
                cy.get('@target-field').find('select').select(fieldValue)
                break
        }
    })
});
