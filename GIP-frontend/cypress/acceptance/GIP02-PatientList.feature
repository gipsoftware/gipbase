@GIP02
Feature: [GIP02]-[STORY] There is a page to manage Patients with the next base elements. The search bar panel, the verbs panel and the patient list panel.

        The Patient List page should have some additional components.
        One Search Bar to select what list of Patients should be shown.
        A list of verbs to be applied to any of the shown Patients.
        And a list of Patients that match the filter selection.

    Background: Application landing page
        Given the application GIPBase
        When the target page is "Patients/List"

    # - H A P P Y   P A T H
    @GIP02.01
    Scenario: [GIP02.01]-Check that the five components that should be visible on the Patient List Page are there and visible.
        # - Check that the expected components are visible
        And the page "Patients List Page" has 5 panels

    @GIP02.02
    Scenario: [GIP02.02]-The initial state for the Patient List is an empty search filter and no Patients. The verb count can change.
        # - Check the Search Bar initial state
        Given the target is the panel of type "Search Bar"
        Then form field named "search-filter" with label "FILTRO:" is empty
        # - Check the Verbs panel initial state
        Given the target is the panel of type "Verbs"
        Then the target has no "verb"
        # - Check that the Patients panel has no patients
        Given the target is the panel of type "Patients"
        Then the target has no "patient"


    @GIP02.03
    Scenario: [GIP02.03]-If the filter is filled with more than 3 characters then the patient list shows some of them.
        # - Set a new search filter
        Given "gar" as search filter
        # - Check that the Patients panel has no patients
        Given the target is the panel of type "Patients"
        Then the target has no "patient"
        # - Set a new search filter
        Given "garc" as search filter
        # - Check that the Patients panel has no patients
        Given the target is the panel of type "Patients"
        Then the target has 50 "patient"

    @GIP02.04
    Scenario: [GIP02.04]-Check that the patient block has the predefined fields and contents.
        # - Select a predefined Patient
        Given the target is the panel of type "Patients"
        Given the target the "patient" with id ""

    @GIP02.05
    Scenario: [GIP02.05]-Check the titles and static texts of the visible components at the Patient List page.
        Given the target is the panel of type "PageHeader"
        Then the target has the title "Selección de Pacientes"
