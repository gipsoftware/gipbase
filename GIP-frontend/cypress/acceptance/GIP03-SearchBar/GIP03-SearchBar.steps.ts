// - CUCUMBER-PREPROCESSOR
import { Given } from "cypress-cucumber-preprocessor/steps";
import { When } from "cypress-cucumber-preprocessor/steps";
import { Then } from "cypress-cucumber-preprocessor/steps";
import { Before } from "cypress-cucumber-preprocessor/steps";
// - SERVICE
import { SupportService } from '../../support/SupportService.support';

const supportService = new SupportService();

// - F I L T E R   T R A N S M I S S I O N   V A L I D A T I O N S
Then('{string} on destination {string} is empty', function (targetField: string, targetPanel: string) {
    const panel = supportService.translateTag(targetPanel) // Do name replacement
    cy.get('@target-page').find(panel)
        .as('target-panel')
    cy.get('@target-panel').find('[cy-name="' + targetField + '"]').as('target-field')
    cy.get('@target-field').should('be.empty')
});
Then('{string} on destination {string} has contents {string}', function (targetField: string, targetPanel: string, contents:string) {
    const panel = supportService.translateTag(targetPanel) // Do name replacement
    cy.get('@target-page').find(panel)
        .as('target-panel')
    cy.get('@target-panel').find('[cy-name="' + targetField + '"]').as('target-field')
    cy.get('@target-field').contains(contents, { matchCase: false })
});

// Before(() => {
//     // cy.log('>Running Before')
//     // cy.visit('/patient/list', {
//     //     onBeforeLoad(win) {
//     //         // start spying
//     //         cy.spy(win, 'postMessage').as('postMessage')
//     //         win.postMessage = (what, target) => {
//     //             cy.log('>What: ' + JSON.stringify(what))
//     //             cy.log('>Target: ' + JSON.stringify(target))
//     //             return postMessage(what, target)
//     //         }
//     //     }
//     // })
//     cy.spy(window, 'postMessage').as('postMessage')
//     window.postMessage = (what, target) => {
//         cy.log('>What: ' + JSON.stringify(what))
//         cy.log('>Target: ' + JSON.stringify(target))
//         return postMessage(what, target)
//     }
// });
// - E V E N T   D E T E C T I O N
Given('a new event observer', function () {
    // cy.get('search-bar-panel').then(($search) => {
    //     cy.spy($search, 'searchThis').as('postMessage')
    // })
});
Then('no event is sent from Filter box', function () {
    cy.get('@postMessage').should('not.have.been.called')
});
Then('event {string} is sent from Filter box', function (string) {
    cy.get('@postMessage').should('have.been.called')
});

Given('{string} is set on form field {string}', function (fieldValue: string, fieldName: string) {
    cy.get('@target-panel').find('[cy-name="' + fieldName + '"]').as('target-field')
    // cy.log('>Setting the spy')
    // cy.spy(window, 'postMessage').as('postMessage')
    // window.postMessage = (what, target) => {
    //     cy.log('>What: ' + JSON.stringify(what))
    //     cy.log('>Target: ' + JSON.stringify(target))
    //     return postMessage(what, target)
    // }
    cy.get('@target-field').find('[cy-field-label="' + fieldName + '"]').invoke('attr', 'cy-input-type').then(type => {
        switch (type) {
            case 'input':
                cy.get('@target-field').find('input').clear().type(fieldValue)
                break
            case 'textarea':
                cy.get('@target-field').find('textarea').clear().type(fieldValue)
                break
            case 'select':
                cy.log('select')
                cy.get('@target-field').find('select').select(fieldValue)
                break
        }
    })
});
