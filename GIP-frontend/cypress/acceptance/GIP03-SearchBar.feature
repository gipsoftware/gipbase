@GIP03
Feature: [GIP03]-[STORY] If the user enters 1 or 2 letters on the filter box then there is no request to the backend.
    [STORY] But if there are 3 or more letters on the Filter then there is a request and the list of matching Patients is shown on the Patients panel.

    The keyboard focus is located on the Filter box at the Search Bar.
    If the user starts keying then the filter contents are edited and an event is sent to report the new Filter contents.
    Events are sent then the number of characters on the filter is 3 or above.

    Background: Application landing page
        Given the application GIPBase
        When the target page is "Patients/List"


    # @GIP03.01
    # Scenario: [GIP03.01]-Check that the focus is located on the Filter box.
    @GIP03.02
    Scenario: [GIP03.02]-Check that an event is sent only when the number of characters on the Filter is grater than 2.
        # - Change the contents of the Filter and verify is an event is sent. 1 character
        Given the target is the panel of type "Search Bar"
        Given "a" is set on form field "search-filter"
        Then "filter" on destination "Patients" is empty
        # - Change the contents of the Filter and verify is an event is sent. 2 character
        Given the target is the panel of type "Search Bar"
        Given "ab" is set on form field "search-filter"
        Then "filter" on destination "Patients" is empty
        # # - Change the contents of the Filter and verify is an event is sent. 3 character
        Given the target is the panel of type "Search Bar"
        Given "abc" is set on form field "search-filter"
        Then "filter" on destination "Patients" has contents "abc"
